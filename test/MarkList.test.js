var MarkList = artifacts.require("MarkList");
var StudentList = artifacts.require("Studentlist");
const SubjectList = artifacts.require('SubjectList')



contract('MarkList', (account) => {
    // make sure contract is deployed
    beforeEach(async () => {
        this.MarkList = await MarkList.deployed()
    })

    it("checke deployed name",async()=>{
        assert.equal(MarkList._json.contractName,"MarkList",
        "marklist contract is correct")
        assert.equal(StudentList._json.contractName,"StudentList",
        "StudentList contract is correct")
        assert.equal(SubjectList._json.contractName,"SubjectList",
        "SubjectList contract is correct")
    })
    it("deployed successfully", async () => {
        // get address of student object
        const address = await this.MarkList.address
        // Test for vaild add ress
        isvalidAddress(address)
    })




})
function isvalidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}