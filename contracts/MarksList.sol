//SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract MarksList {
// Store marks Count
uint256 public marksCount = 0;
enum Grades{ A, B, C, D, FAIL }
// Model a Marks
struct Marks {
uint cid;
string code;
Grades grades;
}
// Store marks
mapping(uint => mapping(string => Marks)) public marks;
// Events
event addMarksEvent (
uint indexed cid,
string indexed code,
Grades grades
);
//Constructor for students
constructor() {
// addMarks( 1001, "CSC101, 1);
}
function addMarks(uint _cid, string memory _code, Grades _grade) public {
//require that Marks with this cid is not added before
require(bytes(marks[_cid][_code].code).length == 0, "Marks not found!");
marksCount++;
marks[_cid][_code] = Marks(_cid, _code, _grade);
emit addMarksEvent(_cid, _code, _grade);
}
// Fetch Marks
function findMarks(uint _cid,string memory _code)
public view returns (Marks memory)
{
return marks[_cid][_code];
}
}
